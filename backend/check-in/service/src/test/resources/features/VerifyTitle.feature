Feature: Verify the Angular application

  Scenario: Verify the title of the Angular application
    Given I open the Angular application
    Then the title of the page should be "BioproExample"
