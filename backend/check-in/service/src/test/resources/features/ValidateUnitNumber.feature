Feature: Check In Blood Product

  Rule: Unit numbers must be exactly 13 characters long.

  Scenario Outline: Validate Unit Number Length
    Given a blood product with unit number: "<unit_number>"
    When the unit number is validated
    Then the validation result should be "<result>"
    And the validation message should be "<message>"

    Examples:
      | unit_number   | result | message                           |
      | A123456789012 | true   | Unit number is valid              |
      | B987654321    | false  | Unit number must be 13 characters |
      | 999           | false  | Unit number must be 13 characters |

#  @Skip
#  Scenario Outline: Validate Unit Number With Check Digit
#    Given a blood product with unit number "<unit_number>" and check digit "<check_digit>"
#    When the unit number with check digit is validated
#    Then the validation result should be "<result>"
#    And the validation message should be "<message>"
#
#    Examples:
#      | unit_number  | check_digit | result | message                          |
#      | A12345678901 | Y           | true   | Unit number is valid             |
#      | B9876543210  | 0           | true   | Unit number is valid             |
#      | C0000000000  | 0           | true   | Unit number is valid             |
#      | D9999999999  | 9           | true   | Unit number is valid             |
#      | A12345678901 | 1           | false  | Unit number has invalid checksum |
