package com.arcone.biopro.example.checkin.unit;

import com.arcone.biopro.example.checkin.adapter.in.web.dto.CheckInProductResponse;
import com.arcone.biopro.example.checkin.domain.service.UnitNumberValidationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

class UnitNumberValidationServiceTest {

    private UnitNumberValidationService unitNumberValidationService;

    @BeforeEach
    void setUp() {
        unitNumberValidationService = new UnitNumberValidationService();
    }

    @Test
    void validateUnitNumberLength_valid() {
        String unitNumber = "1234567890123";
        Mono<CheckInProductResponse> result = unitNumberValidationService.validateUnitNumberLength(unitNumber);
        StepVerifier.create(result)
                .expectNextMatches(validationResult -> validationResult.isValid() && validationResult.getMessage().equals("Unit number is valid"))
                .verifyComplete();
    }

    @Test
    void validateUnitNumberLength_invalid() {
        String unitNumber = "123456";
        Mono<CheckInProductResponse> result = unitNumberValidationService.validateUnitNumberLength(unitNumber);
        StepVerifier.create(result)
                .expectNextMatches(validationResult -> !validationResult.isValid() && validationResult.getMessage().equals("Unit number must be 13 characters"))
                .verifyComplete();
    }
//    @Test
//    void validateUnitNumberWithCheckDigit_valid() {
//        String unitNumber = "1234567890123";
//        String checkDigit = "A";
//        Mono<ValidationResult> result = unitNumberValidationService.validateUnitNumberWithCheckDigit(unitNumber, checkDigit);
//
//        StepVerifier.create(result)
//                .expectNextMatches(validationResult -> validationResult.isValid() && validationResult.getMessage().equals("Unit number is valid"))
//                .verifyComplete();
//    }

    @Test
    void validateUnitNumberWithCheckDigit_invalidLength() {
        String unitNumber = "123456";
        String checkDigit = "A";
        Mono<CheckInProductResponse> result = unitNumberValidationService.validateUnitNumberWithCheckDigit(unitNumber, checkDigit);
        StepVerifier.create(result)
                .expectNextMatches(validationResult -> !validationResult.isValid() && validationResult.getMessage().equals("Unit number must be 13 characters"))
                .verifyComplete();

    }
}