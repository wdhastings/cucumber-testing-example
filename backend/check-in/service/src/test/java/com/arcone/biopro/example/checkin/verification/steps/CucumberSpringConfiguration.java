package com.arcone.biopro.example.checkin.verification.steps;

import com.arcone.biopro.example.checkin.CheckinApplication;
import io.cucumber.spring.CucumberContextConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.TestPropertySource;

@CucumberContextConfiguration
@SpringBootTest(classes= CheckinApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient
@TestPropertySource(locations = "classpath:application-test.properties")
public class CucumberSpringConfiguration {

    @DynamicPropertySource
    static void dynamicProperties(DynamicPropertyRegistry registry) {
        String testHost = System.getenv("TEST_HOST");
        String testPort = System.getenv("TEST_PORT");

        if (testHost != null && testPort != null) {
            registry.add("test.host", () -> testHost);
            registry.add("test.port", () -> testPort);
        }
    }
}
