package com.arcone.biopro.example.checkin.verification.steps;

import com.arcone.biopro.example.checkin.adapter.in.web.dto.CheckInProductRequest;
import com.arcone.biopro.example.checkin.adapter.in.web.dto.CheckInProductResponse;
import com.arcone.biopro.example.checkin.verification.support.ScenarioContext;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.graphql.test.tester.GraphQlTester;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ContextConfiguration(classes = CucumberSpringConfiguration.class)
public class ValidateUnitNumberSteps {

    private final ScenarioContext scenarioContext;
    private final GraphQlTester graphQlTester;

    public ValidateUnitNumberSteps(ScenarioContext scenarioContext, GraphQlTester graphQlTester) {
        this.scenarioContext = scenarioContext;
        this.graphQlTester = graphQlTester;
    }

    @Given("a blood product with unit number: {string}")
    public void a_blood_product_with_unit_number(String unitNumber) {
        unitNumber = unitNumber.trim();
        CheckInProductRequest request = new CheckInProductRequest(unitNumber, null);
        scenarioContext.setRequest(request);
    }

    @Given("a blood product with unit number {string} and check digit {string}")
    public void aBloodProductWithUnitNumberAndCheckDigit(String unitNumber, String checkDigit) {
        unitNumber = unitNumber.trim();
        checkDigit = checkDigit.trim();
        CheckInProductRequest request = new CheckInProductRequest(unitNumber, checkDigit);
        scenarioContext.setRequest(request);
    }

    @When("the unit number is validated")
    public void theUnitNumberIsValidated() {
        CheckInProductRequest request = scenarioContext.getRequest();
        String query = String.format("""
                mutation {
                    checkInProduct(unitNumber: "%s", checkDigit: null) {
                        isValid
                        message
                    }
                }
                """, request.getUnitNumber());
        graphQlTester.document(query)
                .execute()
                .path("checkInProduct")
                .entity(CheckInProductResponse.class)
                .satisfies(scenarioContext::setValidationResult);
    }

    @When("the unit number with check digit is validated")
    public void theUnitNumberWithCheckDigitIsValidated() {
        CheckInProductRequest request = scenarioContext.getRequest();
        String query = String.format("""
                mutation {
                    checkInProduct(unitNumber: "%s", checkDigit: "%s") {
                        isValid
                        message
                    }
                }
                """, request.getUnitNumber(), request.getCheckDigit());
        graphQlTester.document(query)
                .execute()
                .path("checkInProduct")
                .entity(CheckInProductResponse.class)
                .satisfies(scenarioContext::setValidationResult);
    }

    @Then("the validation result should be {string}")
    public void theValidationResultShouldBe(String expectedResult) {
        CheckInProductResponse validationResult = scenarioContext.getValidationResult();
        assertNotNull(validationResult, "Validation result should not be null");
        boolean expectedIsValid = Boolean.parseBoolean(expectedResult);
        assertTrue(expectedIsValid == validationResult.isValid(), "Validation result (isValid) should match");
    }

    @Then("the validation message should be {string}")
    public void theValidationMessageShouldBe(String expectedMessage) {
        CheckInProductResponse validationResult = scenarioContext.getValidationResult();
        assertNotNull(validationResult, "Validation result should not be null");
        assertEquals(expectedMessage, validationResult.getMessage(), "Validation message should match");
    }
}
