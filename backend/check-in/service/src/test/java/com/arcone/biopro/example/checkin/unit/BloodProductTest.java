package com.arcone.biopro.example.checkin.unit;

import com.arcone.biopro.example.checkin.domain.model.BloodProduct;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class BloodProductTest {

    @Test
    void shouldCreateBloodProductWithUnitNumber() {
        String unitNumber = "A123456789012"; // Example valid unit number

        BloodProduct bloodProduct = new BloodProduct(unitNumber);

        assertNotNull(bloodProduct, "Blood product should not be null");
        assertEquals(unitNumber, bloodProduct.getUnitNumber(), "Unit number should be set correctly");
    }
}
