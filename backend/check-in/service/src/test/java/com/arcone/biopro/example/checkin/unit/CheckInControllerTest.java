package com.arcone.biopro.example.checkin.unit;

import com.arcone.biopro.example.checkin.adapter.in.web.controller.CheckInController;
import com.arcone.biopro.example.checkin.adapter.in.web.dto.CheckInProductResponse;
import com.arcone.biopro.example.checkin.domain.service.UnitNumberValidationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class CheckInControllerTest {

    @Mock
    private UnitNumberValidationService unitNumberValidationService;

    private CheckInController checkInController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        // Manually create an instance of CheckInController with the required boolean value
        checkInController = new CheckInController(unitNumberValidationService, true);
    }

    @Disabled
    @Test
    void checkInProduct_validUnitNumberWithoutCheckDigit() {
        CheckInProductResponse lengthValidationResponse = new CheckInProductResponse(true, "Unit number is valid");
        when(unitNumberValidationService.validateUnitNumberLength(anyString()))
                .thenReturn(Mono.just(lengthValidationResponse));
        Mono<CheckInProductResponse> result = checkInController.checkInProduct("1234567890123", null);
        StepVerifier.create(result)
                .expectNextMatches(response -> response.isValid() && "Unit number is valid".equals(response.getMessage()))
                .verifyComplete();
    }

    @Test
    void checkInProduct_invalidUnitNumberLength() {
        CheckInProductResponse lengthValidationResponse = new CheckInProductResponse(false, "Unit number must be 13 characters");
        when(unitNumberValidationService.validateUnitNumberLength(anyString()))
                .thenReturn(Mono.just(lengthValidationResponse));
        Mono<CheckInProductResponse> result = checkInController.checkInProduct("123456", null);
        StepVerifier.create(result)
                .expectNextMatches(response -> !response.isValid() && "Unit number must be 13 characters".equals(response.getMessage()))
                .verifyComplete();
    }

    @Test
    void checkInProduct_checkDigitRequiredButNotProvided() {
        CheckInProductResponse lengthValidationResponse = new CheckInProductResponse(true, "Unit number is valid");
        when(unitNumberValidationService.validateUnitNumberLength(anyString()))
                .thenReturn(Mono.just(lengthValidationResponse));
        Mono<CheckInProductResponse> result = checkInController.checkInProduct("1234567890123", null);
        StepVerifier.create(result)
                .expectNextMatches(response -> !response.isValid() && "Check digit is required but not provided".equals(response.getMessage()))
                .verifyComplete();
    }

    @Test
    void checkInProduct_validUnitNumberWithCheckDigit() {
        CheckInProductResponse lengthValidationResponse = new CheckInProductResponse(true, "Unit number is valid");
        CheckInProductResponse checkDigitValidationResponse = new CheckInProductResponse(true, "Unit number is valid");
        when(unitNumberValidationService.validateUnitNumberLength(anyString()))
                .thenReturn(Mono.just(lengthValidationResponse));
        when(unitNumberValidationService.validateUnitNumberWithCheckDigit(anyString(), anyString()))
                .thenReturn(Mono.just(checkDigitValidationResponse));
        Mono<CheckInProductResponse> result = checkInController.checkInProduct("1234567890123", "A");
        StepVerifier.create(result)
                .expectNextMatches(response -> response.isValid() && "Unit number is valid".equals(response.getMessage()))
                .verifyComplete();
    }

    @Test
    void checkInProduct_invalidCheckDigit() {
        CheckInProductResponse lengthValidationResponse = new CheckInProductResponse(true, "Unit number is valid");
        CheckInProductResponse checkDigitValidationResponse = new CheckInProductResponse(false, "Unit number has invalid checksum");
        when(unitNumberValidationService.validateUnitNumberLength(anyString()))
                .thenReturn(Mono.just(lengthValidationResponse));
        when(unitNumberValidationService.validateUnitNumberWithCheckDigit(anyString(), anyString()))
                .thenReturn(Mono.just(checkDigitValidationResponse));
        Mono<CheckInProductResponse> result = checkInController.checkInProduct("1234567890123", "B");
        StepVerifier.create(result)
                .expectNextMatches(response -> !response.isValid() && "Unit number has invalid checksum".equals(response.getMessage()))
                .verifyComplete();
    }
}
