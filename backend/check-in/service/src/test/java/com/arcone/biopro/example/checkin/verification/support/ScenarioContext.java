package com.arcone.biopro.example.checkin.verification.support;

import com.arcone.biopro.example.checkin.adapter.in.web.dto.CheckInProductRequest;
import com.arcone.biopro.example.checkin.adapter.in.web.dto.CheckInProductResponse;
import io.cucumber.spring.ScenarioScope;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Getter // Lombok annotation to generate getter methods
@Setter // Lombok annotation to generate setter methods
@Component
@ScenarioScope // Use ScenarioScope for Cucumber tests
public class ScenarioContext {
    private CheckInProductRequest request;
    private CheckInProductResponse validationResult;
}
