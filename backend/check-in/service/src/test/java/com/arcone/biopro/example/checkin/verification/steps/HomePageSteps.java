package com.arcone.biopro.example.checkin.verification.steps;

import com.arcone.biopro.example.checkin.verification.pages.HomePage;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import io.github.bonigarcia.wdm.WebDriverManager;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HomePageSteps {

    private WebDriver driver;
    private HomePage homePage;

    @Given("I open the Angular application")
    public void iOpenTheAngularApplication() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get("http://localhost:4200");
        homePage = new HomePage(driver);
    }

    @Then("the title of the page should be {string}")
    public void theTitleOfThePageShouldBe(String expectedTitle) {
        assertEquals(expectedTitle, homePage.getTitle());
    }

    @After
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }
}
