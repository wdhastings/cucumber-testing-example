package com.arcone.biopro.example.checkin.infrastructure.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;

@Configuration
public class SecurityConfig {

    @Bean
    public SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity http) {
        return http
                .csrf(ServerHttpSecurity.CsrfSpec::disable)  // Disable CSRF protection
                .authorizeExchange(exchanges ->
                        exchanges
                                .pathMatchers("/actuator/health").permitAll()
                                .pathMatchers("/graphql").permitAll() // Allow GraphQL requests without authentication
                                .anyExchange().authenticated()
                )
                .build();
    }
}
