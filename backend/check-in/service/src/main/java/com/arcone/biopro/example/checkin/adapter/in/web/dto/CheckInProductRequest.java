package com.arcone.biopro.example.checkin.adapter.in.web.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter // Lombok annotation to generate getter methods
@NoArgsConstructor // Lombok annotation to generate a no-args constructor
@AllArgsConstructor // Lombok annotation to generate an all-args constructor
public class CheckInProductRequest {
    private String unitNumber;
    private String checkDigit;
}
