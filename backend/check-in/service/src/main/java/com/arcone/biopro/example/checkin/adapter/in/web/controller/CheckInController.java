package com.arcone.biopro.example.checkin.adapter.in.web.controller;

import com.arcone.biopro.example.checkin.adapter.in.web.dto.CheckInProductResponse;
import com.arcone.biopro.example.checkin.domain.service.UnitNumberValidationService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Mono;

@Controller
public class CheckInController {

    private final UnitNumberValidationService unitNumberValidationService;
    private final boolean checkDigitRequired;

    public CheckInController(UnitNumberValidationService unitNumberValidationService,
                             @Value("${checkin.check-digit-required}") boolean checkDigitRequired) {
        this.unitNumberValidationService = unitNumberValidationService;
        this.checkDigitRequired = checkDigitRequired;
    }

    @MutationMapping
    public Mono<CheckInProductResponse> checkInProduct(@Argument String unitNumber, @Argument String checkDigit) {
        return unitNumberValidationService.validateUnitNumberLength(unitNumber)
                .flatMap(validationResult -> {
                    if (!validationResult.isValid()) {
                        return Mono.just(validationResult);
                    }
                    if (checkDigitRequired) {
                        if (checkDigit == null || checkDigit.isEmpty()) {
                            return Mono.just(new CheckInProductResponse(false, "Check digit is required but not provided"));
                        }
                        return unitNumberValidationService.validateUnitNumberWithCheckDigit(unitNumber, checkDigit);
                    }
                    return Mono.just(new CheckInProductResponse(true, "Unit number is valid"));
                    // oops.
                });
    }
}
