package com.arcone.biopro.example.checkin.domain.service;

import com.arcone.biopro.example.checkin.adapter.in.web.dto.CheckInProductResponse;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class UnitNumberValidationService {

    public Mono<CheckInProductResponse> validateUnitNumberLength(String unitNumber) {
        return Mono.fromSupplier(() -> {
            boolean isValid = isValidUnitNumberLength(unitNumber);
            String message = isValid ? "Unit number is valid" : "Unit number must be 13 characters";
            return new CheckInProductResponse(isValid, message);
        });
    }

    public Mono<CheckInProductResponse> validateUnitNumberWithCheckDigit(String unitNumber, String checkDigit) {
        return Mono.fromSupplier(() -> {
            boolean isValid = isValidUnitNumberLength(unitNumber) && isValidChecksum(unitNumber, checkDigit);
            String message = isValid ? "Unit number is valid" : getInvalidUnitNumberMessage(unitNumber, checkDigit);
            return new CheckInProductResponse(isValid, message);
        });
    }

    private boolean isValidUnitNumberLength(String unitNumber) {
        return unitNumber != null && unitNumber.length() == 13;
    }

    private boolean isValidChecksum(String unitNumber, String checkCharacter) {
        if (checkCharacter == null || checkCharacter.length() != 1) {
            return false; // Invalid check character format
        }

        int weightedSum = 0;
        for (int i = 0; i < unitNumber.length(); i++) {
            char c = unitNumber.charAt(i);
            int weight = Character.isDigit(c) ? c - '0' : Character.toUpperCase(c) - 'A' + 10;
            weightedSum += weight * (12 - i); // Use reverse index for weighting
        }

        int remainder = weightedSum % 37; // Use 37 as the modulus
        char calculatedCheckCharacter = CHECK_CHARACTERS.charAt(remainder);

        return checkCharacter.equalsIgnoreCase(String.valueOf(calculatedCheckCharacter)); // Case-insensitive comparison
    }

    private String getInvalidUnitNumberMessage(String unitNumber, String checkDigit) {
        if (unitNumber == null || unitNumber.length() != 13) {
            return "Unit number must be 13 characters";
        } else if (checkDigit != null && (checkDigit.length() != 1)) {
            return "Check digit must be a single character";
        } else if (!isValidChecksum(unitNumber.substring(0, unitNumber.length() - 1), checkDigit)) {
            return "Unit number has invalid checksum";
        } else {
            return "Unit number is valid";
        }
    }

    // Check characters table for ISBT 128
    private static final String CHECK_CHARACTERS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ*";
}
