package com.arcone.biopro.example.checkin.infrastructure.config;

import org.springframework.context.annotation.Configuration;

@Configuration
public class GraphQLConfig {
    // No need for manual configuration here,
    // as graphql-spring-boot-starter handles it.
}
