package com.arcone.biopro.example.checkin.adapter.in.web.dto;

import lombok.Getter;

@Getter // Lombok annotation to generate getter methods
public class CheckInProductResponse {
    private final boolean isValid;
    private final String message;

    public CheckInProductResponse(boolean isValid, String message) {
        this.isValid = isValid;
        this.message = message;
    }
    //This is a comment
}
