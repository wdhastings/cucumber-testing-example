package com.arcone.biopro.example.checkin.domain.model;

import lombok.Getter;

@Getter // Lombok annotation to generate the getter method
public class BloodProduct {

    private final String unitNumber;

    public BloodProduct(String unitNumber) {
        this.unitNumber = unitNumber;
    }
}


