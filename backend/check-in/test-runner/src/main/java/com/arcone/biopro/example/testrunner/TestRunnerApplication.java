package com.arcone.biopro.example.testrunner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestRunnerApplication {

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(TestRunnerApplication.class);
        //app.setAdditionalProfiles("test-runner");
        app.run(args);
    }
}
