package com.arcone.biopro.example.testrunner.controller;

import com.arcone.biopro.example.testrunner.service.TestRunnerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/run-tests")
public class TestRunnerController {

    private static final Logger logger = LoggerFactory.getLogger(TestRunnerController.class);
    private final TestRunnerService testRunnerService;

    public TestRunnerController(TestRunnerService testRunnerService) {
        this.testRunnerService = testRunnerService;
    }

    @GetMapping
    public Mono<String> runTests() {
        logger.info("Received request to run tests");
        return testRunnerService.runTests()
                .then(Mono.just("Tests are running"))
                .onErrorResume(e -> {
                    logger.error("Error running tests", e);
                    return Mono.just("Error running tests: " + e.getMessage());
                });
    }
}
