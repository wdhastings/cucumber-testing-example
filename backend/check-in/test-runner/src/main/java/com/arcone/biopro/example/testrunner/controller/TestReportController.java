package com.arcone.biopro.example.testrunner.controller;

import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class TestReportController {

    @GetMapping(value = "/test-report", produces = MediaType.TEXT_HTML_VALUE)
    public Mono<ResponseEntity<FileSystemResource>> getTestReport() {
        return Mono.fromSupplier(() -> {
            FileSystemResource resource = new FileSystemResource("target/cucumber-report/cucumber.html");
            if (resource.exists()) {
                return ResponseEntity.ok().body(resource);
            } else {
                return ResponseEntity.notFound().build();
            }
        });
    }
}
