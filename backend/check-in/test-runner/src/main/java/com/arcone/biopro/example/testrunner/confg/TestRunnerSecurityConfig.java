package com.arcone.biopro.example.testrunner.confg;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;

@Configuration
public class TestRunnerSecurityConfig {

    @Bean
    public SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity http) {
        return http
                .csrf(ServerHttpSecurity.CsrfSpec::disable)  // Disable CSRF protection
                .authorizeExchange(exchanges ->
                        exchanges
                                .anyExchange().permitAll()  // Allow all requests without authentication
                )
                .build();
    }
}
